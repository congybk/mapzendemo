package congybk.com.mapzendemo;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.mapzen.android.graphics.MapFragment;
import com.mapzen.android.graphics.MapzenMap;
import com.mapzen.android.graphics.MapzenMapPeliasLocationProvider;
import com.mapzen.android.graphics.OnMapReadyCallback;
import com.mapzen.android.graphics.model.BubbleWrapStyle;
import com.mapzen.android.graphics.model.CinnabarStyle;
import com.mapzen.android.graphics.model.MapStyle;
import com.mapzen.android.graphics.model.Marker;
import com.mapzen.android.graphics.model.Polyline;
import com.mapzen.android.graphics.model.RefillStyle;
import com.mapzen.android.graphics.model.WalkaboutStyle;
import com.mapzen.android.lost.api.LocationServices;
import com.mapzen.android.lost.api.LostApiClient;
import com.mapzen.android.routing.MapzenRouter;
import com.mapzen.android.search.MapzenSearch;
import com.mapzen.helpers.RouteEngine;
import com.mapzen.helpers.RouteListener;
import com.mapzen.model.ValhallaLocation;
import com.mapzen.pelias.gson.Feature;
import com.mapzen.pelias.gson.Result;
import com.mapzen.pelias.widget.AutoCompleteAdapter;
import com.mapzen.pelias.widget.AutoCompleteListView;
import com.mapzen.pelias.widget.PeliasSearchView;
import com.mapzen.tangram.LngLat;
import com.mapzen.tangram.TouchInput;
import com.mapzen.valhalla.Route;
import com.mapzen.valhalla.RouteCallback;
import com.mapzen.valhalla.Router;


import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity implements  LostApiClient.ConnectionCallbacks, View.OnClickListener,RadioGroup.OnCheckedChangeListener {
    private MapzenMap mMap;
    private boolean mEnableLocationOnResume = false;
    private ProgressBar mProgressBar;
    private AutoCompleteListView mAutoCompleteListView;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private MapzenMapPeliasLocationProvider mMapzenMapPeliasLocationProvider;
    private MapzenSearch mMapzenSearch;
    private LostApiClient mClient;
    private LinearLayout mLlDirection;

    private Location mLocation;
    private LngLat mLngLat;
    private MapzenRouter mRouter;
    private TextView mTvLoaction;
    private ImageView mImageDirection;
    private TextView mTvDistance;
    private TextView mTvTime;
    private LinearLayout mLlInfo;

    private RadioGroup mRadioGroup;
    private RouteEngine mRouteEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        initUi();
        mRouter = new MapzenRouter(this, getString(R.string.vector_tiles_key));
        setUpRouteEngine();

        MapFragment mapFragment = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapzenMap mapzenMap) {
                Log.i("TAG11", "onMapReady");
                configureMap(mapzenMap);
                configureRouter(1);
                clickPositionMap();
                //longPressMap();

            }
        });
        mClient = new LostApiClient.Builder(this).addConnectionCallbacks(this).build();
        this.setUpMapzenSearch();
    }
    /*
    * Long Press Map
    * */
    private void longPressMap() {
        mMap.setLongPressResponder(new TouchInput.LongPressResponder() {
            @Override
            public void onLongPress(float x, float y) {
                LngLat point = mMap.screenPositionToLngLat(new PointF(x, y));
                mMap.drawDroppedPin(point);
            }
        });
    }

    /*
    * Map set Click postion
    * */
    private void clickPositionMap(){
        mMap.setTapResponder(new TouchInput.TapResponder() {
            @Override public boolean onSingleTapUp(float x, float y) {
                final LngLat point = mMap.screenPositionToLngLat(new PointF(x, y));
                mLlInfo.setVisibility(View.GONE);
                mMap.clearRouteLine();
                mMap.removeMarker();
                mRouter.clearLocations();
                Log.i("TAG111",mRouter.getRouter()+"");
                mMap.addMarker(new Marker(point.longitude,point.latitude));
                mMapzenSearch.reverse(point.latitude, point.longitude, new Callback<Result>() {
                    @Override
                    public void success(Result result, Response response) {
                        String region = result.getFeatures().get(0).properties.region;
                        String name = result.getFeatures().get(0).properties.name;
                        mLlDirection.setVisibility(View.VISIBLE);
                        addPointToRoute(mLngLat);
                        addPointToRoute(point);
                        mTvLoaction.setText(name+","+region);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
                return false;
            }

            @Override public boolean onSingleTapConfirmed(float x, float y) {
                return false;
            }
        });
    }

    /*
    *
    * SetUp RouteEngine
    * */

    private void setUpRouteEngine() {
        mRouteEngine = new RouteEngine();
        RouteListener routeListener = new RouteListener() {
            @Override
            public void onRouteStart() {

            }

            @Override
            public void onRecalculate(ValhallaLocation location) {

            }

            @Override
            public void onSnapLocation(ValhallaLocation originalLocation, ValhallaLocation snapLocation) {

            }

            @Override
            public void onMilestoneReached(int index, RouteEngine.Milestone milestone) {

            }

            @Override
            public void onApproachInstruction(int index) {

            }

            @Override
            public void onInstructionComplete(int index) {

            }

            @Override
            public void onUpdateDistance(int distanceToNextInstruction, int distanceToDestination) {

            }

            @Override
            public void onRouteComplete() {

            }
        };
        mRouteEngine.setListener(routeListener);

    }

    /*
    * Init ui
    * */
    private void initUi(){
        mRadioGroup = (RadioGroup)findViewById(R.id.routing_mode);
        mRadioGroup.setOnCheckedChangeListener(this);
        mLlInfo =  (LinearLayout)findViewById(R.id.llInfo);
        mTvDistance = (TextView)findViewById(R.id.tvDistance);
        mTvTime = (TextView)findViewById(R.id.tvTime);
        mImageDirection = (ImageView)findViewById(R.id.imgDirection);
        mImageDirection.setOnClickListener(this);
        mTvLoaction = (TextView)findViewById(R.id.tvLocation);
        mAutoCompleteListView = (AutoCompleteListView) findViewById(R.id.AutoCompleteListView);
        mAutoCompleteAdapter = new AutoCompleteAdapter(this, android.R.layout.simple_list_item_1);
        mAutoCompleteListView.setAdapter(mAutoCompleteAdapter);
        mLlDirection = (LinearLayout)findViewById(R.id.llDirection);
        mMapzenMapPeliasLocationProvider = new MapzenMapPeliasLocationProvider(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
    }
    /*
    *
    * setUp Mapzen Search
    * */
    private void setUpMapzenSearch(){
        mMapzenSearch = new MapzenSearch(this, getString(R.string.vector_tiles_key));
        mMapzenSearch.setLocationProvider(mMapzenMapPeliasLocationProvider);
        PeliasSearchView peliasSearchView = new PeliasSearchView(this);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        getSupportActionBar().setCustomView(peliasSearchView, lp);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        setupPeliasSearchView(peliasSearchView);
    }
    /*
    *
    * setUp Pelias SearchView
    * */
    private void setupPeliasSearchView(final PeliasSearchView searchView) {
        searchView.setAutoCompleteListView(mAutoCompleteListView);
        mAutoCompleteListView.setVisibility(View.VISIBLE);
        mLlInfo.setVisibility(View.GONE);
        searchView.setPelias(mMapzenSearch.getPelias());
        searchView.setAutoCompleteIconResourceId(R.drawable.ic_pin_c);
        searchView.setCallback(new Callback<Result>() {
            @Override
            public void success(Result result, Response response) {
                mMap.clearSearchResults();
                for (Feature feature : result.getFeatures()) {
                    List<Double> coordinates = feature.geometry.coordinates;
                    LngLat point = new LngLat(coordinates.get(0), coordinates.get(1));
                    mMap.drawSearchResult(point);
                    mMap.setPosition(point);
                    mMap.setRotation(0f);
                    mMap.setZoom(17f);
                    mMap.setTilt(0f);
                    mMap.clearRouteLine();
                    mRouter.clearLocations();
                    addPointToRoute(point);
                    addPointToRoute(mLngLat);
                    mLlInfo.setVisibility(View.GONE);
                    mLlDirection.setVisibility(View.VISIBLE);
                    mMap.setMyLocationEnabled(false);
                    mTvLoaction.setText(searchView.getQuery());

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(this.getString(R.string.search_hint));
        searchView.setOnBackPressListener(new PeliasSearchView.OnBackPressListener() {
            @Override
            public void onBackPressed() {
                mMap.clearSearchResults();
            }
        });
    }
    /**
     * Configure Map
     * */
    private void configureMap(MapzenMap mapzenMap){
        mMap = mapzenMap;
        mapzenMap.setPersistMapData(true);
        mProgressBar.setVisibility(View.GONE);
        mapzenMap.setMyLocationEnabled(true);
        mMap.setPersistMapData(true);
        mMap.setZoom(15f);
        mMap.setPosition(mLngLat);
    }
    /*
    *
    * Configure Router
    * */
    private void configureRouter(int vehicle) {
        switch (vehicle){
            case 1:
                mRouter.setDriving();
                break;
            case 2:
                mRouter.setBiking();
                break;
            case 3:
                mRouter.setWalking();
                break;
            case 4:
                mRouter.setMultimodal();
                break;
        }
        mRouter.setCallback(new RouteCallback() {
            @Override public void success(Route route) {
                List<LngLat> coordinates = new ArrayList<>();
                Log.i("TAG11",route.getRawRoute()+"");
                for (ValhallaLocation location : route.getGeometry()) {
                    coordinates.add(new LngLat(location.getLongitude(), location.getLatitude()));
                }
                int time = route.getTotalTime()/60;
                mTvTime.setText("Số phút: "+time);
                mTvDistance.setText("Số kilomet: "+route.getTotalDistance()/1000);

                mMap.drawRouteLine(coordinates);
            }

            @Override public void failure(int i) {
                Log.d("Fail", "Failed to get route");
            }
        });
    }

    /*
    *
    * Add Point to Route
    * */

    private void addPointToRoute(LngLat lngLat){
        double[] point = {lngLat.latitude, lngLat.longitude};
        mRouter.setLocation(point);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_bubble:
                changeMapStyle(new BubbleWrapStyle());
                break;
            case R.id.item_cinnabar:
                changeMapStyle(new CinnabarStyle());
                break;
            case R.id.item_refill:
                changeMapStyle(new RefillStyle());
                break;
            case R.id.item_walkabout:
                changeMapStyle(new WalkaboutStyle());
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mMap.isMyLocationEnabled()) {
            mMap.setMyLocationEnabled(false);
            mEnableLocationOnResume = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mEnableLocationOnResume) {
            mMap.setMyLocationEnabled(true);
        }
    }

    private void changeMapStyle(MapStyle style) {
        if (mMap != null) {
            mMap.setStyle(style);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMap.setPersistMapData(false);
    }

    @Override
    public void onConnected() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {

            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mClient);
        if (mLocation != null) {
            mLngLat = new LngLat(mLocation.getLongitude(), mLocation.getLatitude());

        } else {
            Log.i("TAG111", "YYYYYYYYYYYYYYYYY");
        }

    }

    @Override
    public void onConnectionSuspended() {
        mClient.connect();
    }

    @Override
    protected void onStart() {
        Log.i("TAG11", "Start");
        super.onStart();
        mClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mClient.isConnected()) {
            mClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mClient.connect();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgDirection:
                mRouter.fetch();
                mMap.drawRouteLocationMarker(mLngLat);
                mMap.setMyLocationEnabled(false);
                mLlInfo.setVisibility(View.VISIBLE);
                mMap.setMyLocationEnabled(true);
                mLlDirection.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.by_car:
                mMap.clearRouteLine();
                configureRouter(1);
                mRouter.fetch();
                break;
            case R.id.by_bike:
                mMap.clearRouteLine();
                configureRouter(2);
                mRouter.fetch();
                break;
            case R.id.by_foot:
                mMap.clearRouteLine();
                configureRouter(3);
                mRouter.fetch();
                break;
            case R.id.by_transit:
                mMap.clearRouteLine();
                configureRouter(4);
                mRouter.fetch();
                break;
        }
    }
}
